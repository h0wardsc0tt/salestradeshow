﻿//Number.prototype.format(n, x) 
//param integer n: length of decimal
//param integer x: length of sections
Number.prototype.format = function (n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};
jQuery.fn.center = function () {
    this.css("position", "absolute");
    this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) +
                                                $(window).scrollTop()) + "px");
    this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) +
                                                $(window).scrollLeft()) + "px");
    return this;
}

var order_page = 1;
var cart = [];
$(document).ready(function () {
    console.log(jsonTradeShowObj);

    setTimeout(function () {
        $('#orderlist').trigger("click");
    }, 500);
   
    //populatecategory('order');
    //console.log(jsonTradeShowObj.productpricing[0].price.format(2));
    //$.each(jsonTradeShowObj.productpricing, function (index, product) {
    //    console.log(product.price.format(2));
    //});

    //var product = alasql("select * from ? where itemnumber = 'C40000-5-HS3-MCD'", [products.productpricing]);
    //console.log(product);

    //var state = alasql("select state,territory from ? where state = 'CA'", [products.states]);
    //console.log(state);

    //var salesrep = alasql("select * from ? where salesrep_territory_id = '" + state[0].territory + "'", [products.salesreps]);
    //console.log(salesrep);

    //var client = new ClientJS();
    //var fingerprint = client.getFingerprint();
    //console.log(fingerprint);

    //var storelookup = {};
    //storelookup.status = '';
    //storelookup.storelookup = {};
    //storelookup.storelookup.storenumber = '10561';
    //$.ajax({
    //    type: "POST",
    //    async: true,
    //    url: "/Home/StoreLookup",
    //    data: JSON.stringify(storelookup),
    //    contentType: "application/json; charset=utf-8",
    //    dataType: "json",
    //    success: function (dto) {
    //        console.log(dto);
    //    }
    //});

    //storelookup = {};
    //storelookup.status = '';
    //storelookup.storelookup = {};
    //storelookup.storelookup.avantebill = '107783';
    //$.ajax({
    //    type: "POST",
    //    async: true,
    //    url: "/Home/StoreLookup",
    //    data: JSON.stringify(storelookup),
    //    contentType: "application/json; charset=utf-8",
    //    dataType: "json",
    //    success: function (dto) {
    //        console.log(dto);
    //    }
    //});

    //populateProducts();

    $('#productCategory').on('change', function (e) {
        console.log('#productCategory')
        var products = alasql("select * from ? where code = '" + $(this).val() + "'", [jsonTradeShowObj.productpricing]);
        $('#products').find('option').not(':first').remove();
        $.each(products, function (index, item) {
            $("<option />", {
                val: item.shortdescription,
                text: item.shortdescription,
                itemnumber: item.itemnumber,
                description: item.description,
                price: item.price
            }).appendTo($('#products'));
        });

    });
  
    var jsonProductCartItem = null;
    $('#products').on('change', function (e) {
        jsonProductCartItem = {};
        jsonProductCartItem.ordered = parseInt($('option:selected', $('#product_amount_ordered')).val());
        jsonProductCartItem.price = parseInt($('option:selected', this).attr('price'));
        jsonProductCartItem.itemnumber = $('option:selected', this).attr('itemnumber');
        jsonProductCartItem.description = $('option:selected', this).attr('description');
        $('#product_description').html(jsonProductCartItem.description);
        $('#product_price').html('$' + jsonProductCartItem.price.format(2));
        $('#product_total').html('$' + (jsonProductCartItem.ordered * jsonProductCartItem.price).format(2));
    });

    $('#product_amount_ordered').on('change', function (e) {
        if (jsonProductCartItem == null)
            return;
        jsonProductCartItem.ordered = parseInt($('option:selected', this).val());
        $('#product_total').html('$' + (jsonProductCartItem.ordered * jsonProductCartItem.price).format(2));
    });

    $("#orderlist").on("click", function () {
        console.log('orderlist')
        populatecategory('1');
    });

    $('#servicelist').click(function () {
        populatecategory('2');
    });

    $('#bothlist').click(function () {
        populatecategory('3');
    });

    $('#resetbtn').click(function () {
        jsonProductCartItem = null;
        $('.row.order .dev-box').html('');
        $('.row.order .dropdown').val($('.row.order .dropdown option:first').val());
        $('#product_amount_ordered').val('1');
    });


    $('#addtocartbtn').click(function () {
        if ($('option:selected', $('#storeNumber')).val() == '') {
            displaymodalmessage('Please select a store number.', -1);
            return;
        }
        if (jsonProductCartItem == null) {
            displaymodalmessage('Please select a product you wish to purchase.', -1);
            return;
        }

        $('.row.order .dev-box').html('');
        $('.row.order .dropdown').val($('.row.order .dropdown option:first').val());
        $('#product_amount_ordered').val('1');
        jsonProductCartItem.store = $('option:selected', $('#storeNumber')).val();
        cart.push(jsonProductCartItem);
        jsonProductCartItem = null;
        console.log(cart);
    });

    // Defaults
    var order_page_total = $('.order-step').length;
    $('.order-step').hide();
    $('.order-step-1').show();
    $('.service-cont').hide();
    $('.order-final-step').hide();
    $('.order-confirm').hide();

    // Order Type
    $('.order-type div input').click(function () {
        $('.order-type div input').each(function () {
            $(this).removeClass("btn-primary");
            $(this).addClass("btn-default");
        });
        $(this).removeClass("btn-default");
        $(this).addClass("btn-primary");

        if($(this).val() === "Order") {
            $('.service-cont').hide();
            $('.order-cont').show();
        }
        if ($(this).val() === "Service") {
            $('.order-cont').hide();
            $('.service-cont').show();
        }
        if ($(this).val() === "Both") {
            $('.order-cont').show();
            $('.service-cont').show();
        }
    });

    // Previous Button
    $('.order-prev-step').click(function () {
        order_page--;
        $('.order-next-step').prop("disabled", false);
        if (order_page === 1) {
            $('.order-prev-step').prop("disabled", true);
        }
        $('.order-final-step').hide();
        $('.order-next-step').show();
        $('.order-step').hide();
        $('.order-step-' + order_page).show();
    });

    // Next Button
    $('.order-next-step').click(function () {
        order_page++;
        $('.order-prev-step').prop("disabled", false);
        if (order_page === order_page_total) {
            $('.order-next-step').prop("disabled", true);
            $('.order-next-step').hide();
            $('.order-final-step').show();
        }
        formSubmit();
        $('.order-step').hide();
        $('.order-step-' + order_page).show();
    });

    // Breadcrumbs
    $('li.breadcrumb-item a').click(function () {
        order_page = $(this).attr("id");
        $('.order-next-step').prop("disabled", false);
        $('.order-prev-step').prop("disabled", false);
        $('.order-final-step').hide();
        $('.order-next-step').show();
        $('.order-step').hide();
        $('.order-step-' + order_page).show();

        if ($(this).attr("id") == 1) {
            $('.order-prev-step').prop("disabled", true);
        }
    });

    // Form Submit
    $('#order-form').submit(function () {

        // Insert Order to Database

        $('.order-step').hide();
        $('.order-nav-footer').hide();
        $('.order-confirm').show();

        setTimeout(function() {
            location.reload();
        }, 5000);

        return false;
    });

    
    $('#ownerlookup').click(function () {
        if ($('#Order_BillTo').val().length >= 3) {
            storelookup = {};
            storelookup.status = '';
            storelookup.storelookup = {};
            //storelookup.storelookup.avantebill = $('#Order_BillTo').val();
            switch ($("input[name='lookupBy']:checked").val()) {
                case 'store':
                    storelookup.storelookup.storenumber = $('#Order_BillTo').val();
                    break;
                case 'zip':
                    storelookup.storelookup.sitezip = $('#Order_BillTo').val();
                    break;
              default:
                  storelookup.storelookup.storenumber = $('#Order_BillTo').val();
                  break;

            }
            $.ajax({
                type: "POST",
                async: true,
                url: "/Home/StoreLookup",
                data: JSON.stringify(storelookup),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (dto) {
                    if (dto.status == 'OK') {
                        console.log(dto);
                        $('#Order_Contact').val(dto.storeslist.billing[0].billingcontactname);
                        $('#Street_Address').val(dto.storeslist.billing[0].billingaddress);
                        $('#Order_Postal').val(dto.storeslist.billing[0].billingzipcode);
                        $('#Order_City').val(dto.storeslist.billing[0].billingcity);
                        $('#Order_Region').val(dto.storeslist.billing[0].billingstate);
                        $('#storeNumber').find('option').not(':first').remove();
                        $.each(dto.storeslist.stores, function (index, item) {
                            $("<option />", {
                                val: item.storenumber,
                                text: item.storenumber + ' - ' + item.siteaddress.replace(/&+/g, '') + ',' + item.sitecity + ',' + item.sitestate,
                                avante: item.avante,
                                avantebill: item.avantebill
                            }).appendTo($('#storeNumber'));
                        });
                       // $('#storeNumber').SumoSelect();
                    }
                }
            });
        } else {
            $('.billing').val('');
            $('#storeNumber').find('option').not(':first').remove();
        }
    });

});

function formSubmit() {
    if (order_page === 3) {
        var total = 0;
        var selected = [];
        var selectedProducts = [];
        $('.order-cont .form-group .col-lg-12.show input:checked').each(function () {
            selected.push({ product: $(this).attr('id') });
            selectedProducts.push(alasql("select * from ? where itemnumber = '" + $(this).attr('id') + "'", [jsonTradeShowObj.productpricing])[0]);
        });
        console.log(selected);
        console.log(selectedProducts);
        $.each(selectedProducts, function (index, product) {
            total += product.price;
        });

        console.log('$' + total.format(2));

    }
}

function populateProducts() {
    var productList = '';
    $.each(jsonTradeShowObj.productpricing, function (index, product) {
        productList += '<div class="col-lg-12 show ' + product.code.replace(/\s+/g, '-') + '" code="' + product.code + '"><label for="' + product.itemnumber + '">' + product.shortdescription + '</label><input type="checkbox" id="' + product.itemnumber + '" value=""></div>';
    });
    //$('.order-cont .form-group').html(productList);
}

function displaySignatureErrorMsg(msg) {
    $('#signaturemsg').html(msg);
}

function displaySignatureImage(img) {
    $('#signatureImage').attr('src', 'data:image/png;base64,' + img);
}

function clearSignatureImage() {
    $('#signatureImage').attr('src','');
}

function closeResetsignature() {
    setTimeout(function () {
        $('#signature_Iframe')[0].contentWindow.signatureCloseReset();
    }, 1000);
}

function populatecategory(type) {
    var reqType = '';
    switch (type) {
        case '1':
            reqType = " where producttype = '1'";
            break;
        case '2':
            reqType = " where producttype = '2'";
            break;
    }
    var category = alasql("select distinct code from ?" + reqType, [jsonTradeShowObj.productpricing]);
    console.log(category);
    $('#products').find('option').not(':first').remove();
    $('#productCategory').find('option').not(':first').remove();
    $.each(category, function (index, item) {
        $("<option />", {
            val: item.code,
            text: item.code
        }).appendTo($('#productCategory'));
    });
    jsonProductCartItem = null;
    $('.row.order .dev-box').html('');
    $('.row.order .dropdown').val($('.row.order .dropdown option:first').val());
    $('#product_amount_ordered').val('1');
}

function displaymessage(msg, closetime) {
    $('#msg').html(msg);
    $('#message').center();
    $('#message').slideDown("slow", function () {
        if (closetime > 0) {
            setTimeout(function () {
                $('#message').slideUp("slow", function () {
                });
            }, closetime);
        }
    });
}

function displaymodalmessage(msg, closetime) {
    $('.alerteModalMsg').html(msg);
    $('#alerteModal').modal('show');
    if (closetime > 0) {
        setTimeout(function () {
            $('#alerteModal').modal('hide');
        }, closetime);
    }
}


