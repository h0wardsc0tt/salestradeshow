﻿using HME.SQLSERVER.DAL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;

namespace SalesTradeShow.Models
{
    public class HomeModels
    {
        public startup_dto jsonStartup()
        {
            startup_dto dto = new startup_dto();
            dbSQL db = new dbSQL(ConfigurationManager.ConnectionStrings["powsql"].ConnectionString);
            string sqlMessage = string.Empty;
            DataSet ds;
            List<SqlParameter> sp = new List<SqlParameter>();
            sp.Add(new SqlParameter("id", null));
            sp.Add(new SqlParameter("itemnumber", null));

            ds = db.executeStoredProcedure("get_ProductPricing", sp, out sqlMessage);
            db.Dispose();

            List<productpricing> productpricinglist = new List<productpricing>();
            productpricinglist = ds.Tables[0].AsEnumerable().Select(m => new productpricing()
            {
                id = Convert.ToString(m["id"]),
                sortorder = Convert.ToString(m["sortorder"]),
                code = Convert.ToString(m["code"]),
                shortdescription = Convert.ToString(m["shortdescription"]),
                itemnumber = Convert.ToString(m["itemnumber"]),
                description = Convert.ToString(m["description"]),
                //producttype = Convert.ToString(m["producttype"]),
                price = Convert.ToDecimal(m["price"])
            }).ToList();

            List<salesreps> salesrepslist = new List<salesreps>();
            salesrepslist = ds.Tables[1].AsEnumerable().Select(m => new salesreps()
            {
                salesrep_id = Convert.ToString(m["SalesRep_UID"]),
                salesrep_uid = Convert.ToString(m["SalesRep_UID"]),
                salesrep_firstname = Convert.ToString(m["SalesRep_FirstName"]),
                salesrep_lastname = Convert.ToString(m["SalesRep_LastName"]),
                salesrep_email = Convert.ToString(m["SalesRep_Email"]),
                salesrep_territory_id = Convert.ToString(m["SalesRep_Territory_ID"]),
                salesrep_type = Convert.ToString(m["SalesRep_Type"])
            }).ToList();

            List<states> stateslist = new List<states>();
            stateslist = ds.Tables[2].AsEnumerable().Select(m => new states()
            {
                state = Convert.ToString(m["abbreviation"]),
                territory = Convert.ToString(m["territoryid"]),
            }).ToList();


            dto.status = "OK";
            dto.productpricing = productpricinglist;
            dto.salesreps = salesrepslist;
            dto.states = stateslist;

            return dto;
        }

        public storesdto StoreLookup(storesdto dto)
        {
            dbSQL db = new dbSQL(ConfigurationManager.ConnectionStrings["powsql"].ConnectionString);
            string sqlMessage = string.Empty;
            DataSet ds;
            List<SqlParameter> sp = new List<SqlParameter>();
            Type myClassType = dto.storelookup.GetType();
            PropertyInfo[] properties = myClassType.GetProperties();
            object value = string.Empty;

            foreach (PropertyInfo property in properties as IEnumerable)
            {
                value = property.GetValue(dto.storelookup, null);
                sp.Add(new SqlParameter(property.Name, value));
            }

            storeslist storeslist = new storeslist();
            dto.storeslist = storeslist;
            ds = db.executeStoredProcedure("select_store", sp, out sqlMessage);
            db.Dispose();
            dto.status = sqlMessage;

            if (sqlMessage == "OK")
            {
                List<storeinfo> storelist = new List<storeinfo>();
                storelist = ds.Tables[0].AsEnumerable().Select(m => new storeinfo()
                {
                    storenumber = Convert.ToString(m["NATL_STR_NU"]),
                    avante = Convert.ToString(m["AVANTE"]),
                    avantebill = Convert.ToString(m["AVANTE_BILL_TO"]),
                    siteaddress = Convert.ToString(m["SITE_ADDRESS"]),
                    sitecity = Convert.ToString(m["SITE_CITY"]),
                    sitestate = Convert.ToString(m["SITE_STATE"]),
                    sitezip = Convert.ToString(m["SITE_ZIP"]),
                    sitephne = Convert.ToString(m["SITE_PHNE"]),
                    operfirstname = Convert.ToString(m["OPER_FirstName"]),
                    operlastname = Convert.ToString(m["OPER_LastName"]),
                    corp = Convert.ToString(m["CORP"]),
                    operaddress = Convert.ToString(m["OPER_ADDRESS"]),
                    opercity = Convert.ToString(m["OPER_CITY"]),
                    operstate = Convert.ToString(m["OPER_STATE"]),
                    operzip = Convert.ToString(m["OPER_ZIP"]),
                    operidnu = Convert.ToString(m["OPER_ID_NU"]),
                    //billingcustomername = Convert.ToString(m["CUSTOMERNAME"]),
                    //billingstorenumber = Convert.ToString(m["STORENUMBER"]),
                    //billingcity = Convert.ToString(m["ADDRESSCITY"]),
                    //billingstate = Convert.ToString(m["ADDRESSSTATE"]),
                    //billingcountry = Convert.ToString(m["ADDRESSCOUNTRY"]),
                    //billingzipcode = Convert.ToString(m["ADDRESSZIPCODE"]),
                    //billingaddress = Convert.ToString(m["ADDRESS"]),
                    //billingcontactname = Convert.ToString(m["CONTACTNAME"]),
                    //billingcontactphone = Convert.ToString(m["CONTACTPHONE"]),
                    //billingcontactphoneextension = Convert.ToString(m["CONTACTPHONEEXTENSION"])
                 }).ToList();

                List<storeinfo> additionalstorelist = new List<storeinfo>();
                additionalstorelist = ds.Tables[1].AsEnumerable().Select(m => new storeinfo()
                {
                    storenumber = Convert.ToString(m["NATL_STR_NU"]),
                    avante = Convert.ToString(m["AVANTE"]),
                    avantebill = Convert.ToString(m["AVANTE_BILL_TO"]),
                    siteaddress = Convert.ToString(m["SITE_ADDRESS"]),
                    sitecity = Convert.ToString(m["SITE_CITY"]),
                    sitestate = Convert.ToString(m["SITE_STATE"]),
                    sitezip = Convert.ToString(m["SITE_ZIP"]),
                    sitephne = Convert.ToString(m["SITE_PHNE"]),
                    operfirstname = Convert.ToString(m["OPER_FirstName"]),
                    operlastname = Convert.ToString(m["OPER_LastName"]),
                    corp = Convert.ToString(m["CORP"]),
                    operaddress = Convert.ToString(m["OPER_ADDRESS"]),
                    opercity = Convert.ToString(m["OPER_CITY"]),
                    operstate = Convert.ToString(m["OPER_STATE"]),
                    operzip = Convert.ToString(m["OPER_ZIP"]),
                    operidnu = Convert.ToString(m["OPER_ID_NU"]),
                    //billingcustomername = Convert.ToString(m["CUSTOMERNAME"]),
                    //billingstorenumber = Convert.ToString(m["STORENUMBER"]),
                    //billingcity = Convert.ToString(m["ADDRESSCITY"]),
                    //billingstate = Convert.ToString(m["ADDRESSSTATE"]),
                    //billingcountry = Convert.ToString(m["ADDRESSCOUNTRY"]),
                    //billingzipcode = Convert.ToString(m["ADDRESSZIPCODE"]),
                    //billingaddress = Convert.ToString(m["ADDRESS"]),
                    //billingcontactname = Convert.ToString(m["CONTACTNAME"]),
                    //billingcontactphone = Convert.ToString(m["CONTACTPHONE"]),
                    //billingcontactphoneextension = Convert.ToString(m["CONTACTPHONEEXTENSION"])
                }).ToList();

                List<billinginfo> billing = new List<billinginfo>();
                billing = ds.Tables[2].AsEnumerable().Select(m => new billinginfo()
                {
                    billingnumber = Convert.ToString(m["CUSTOMERID"]),
                    billingcustomername = Convert.ToString(m["CUSTOMERNAME"]),
                    billingstorenumber = Convert.ToString(m["STORENUMBER"]),
                    billingcity = Convert.ToString(m["ADDRESSCITY"]),
                    billingstate = Convert.ToString(m["ADDRESSSTATE"]),
                    billingcountry = Convert.ToString(m["ADDRESSCOUNTRY"]),
                    billingzipcode = Convert.ToString(m["ADDRESSZIPCODE"]),
                    billingaddress = Convert.ToString(m["ADDRESS"]),
                    billingcontactname = Convert.ToString(m["CONTACTNAME"]),
                    billingcontactphone = Convert.ToString(m["CONTACTPHONE"]),
                    billingcontactphoneextension = Convert.ToString(m["CONTACTPHONEEXTENSION"])
                }).ToList();
                dto.storeslist.billing = billing;
                dto.storeslist.stores = storelist;
                dto.storeslist.additionalstores = additionalstorelist;
             }
            return dto;
        }

        [Serializable]
        public class dto
        {
            public string image { get; set; }
            public string width { get; set; }
            public string height { get; set; }
            public string id { get; set; }
        }
        [Serializable]
        public class startup_dto
        {
            public List<productpricing> productpricing { get; set; }
            public string status { get; set; }
            public List<salesreps> salesreps { get; set; }
            public List<states> states { get; set; }
        }

        [Serializable]
        public class productpricing
        {
            public string id { get; set; }
            public string sortorder { get; set; }
            public string code { get; set; }
            public string shortdescription { get; set; }
            public string itemnumber { get; set; }
            public string description { get; set; }
            public decimal price { get; set; }
            //public string producttype { get; set; }
        }

        [Serializable]
        public class salesreps
        {
            public string salesrep_id { get; set; }
            public string salesrep_uid { get; set; }
            public string salesrep_firstname { get; set; }
            public string salesrep_lastname { get; set; }
            public string salesrep_email { get; set; }
            public string salesrep_territory_id { get; set; }
            public string salesrep_type { get; set; }
        }

        [Serializable]
        public class states
        {
            public string state { get; set; }
            public string territory { get; set; }
        }

        public class storesdto
        {
            public storeinfo storelookup { get; set; }
            public storeslist storeslist { get; set; }
            public string status { get; set; }
        }


        public class storeslist
        {
            public List<storeinfo> stores { get; set; }
            public List<billinginfo> billing { get; set; }
            public List<storeinfo> additionalstores { get; set; }
        }


        [Serializable]
        public class storeinfo
        {
            public string storenumber { get; set; }
            public string avante { get; set; }
            public string avantebill { get; set; }
            public string siteaddress { get; set; }
            public string sitecity { get; set; }
            public string sitestate { get; set; }
            public string sitezip { get; set; }
            public string sitephne { get; set; }
            public string operfirstname { get; set; }
            public string operlastname { get; set; }
            public string corp { get; set; }
            public string operaddress { get; set; }
            public string opercity { get; set; }
            public string operstate { get; set; }
            public string operzip { get; set; }
            public string operidnu { get; set; }
            //public string billingcustomername { get; set; }
            //public string billingstorenumber { get; set; }
            //public string billingcity { get; set; }
            //public string billingstate { get; set; }
            //public string billingcountry { get; set; }
            //public string billingzipcode { get; set; }
            //public string billingaddress { get; set; }
            //public string billingcontactname { get; set; }
            //public string billingcontactphone { get; set; }
            //public string billingcontactphoneextension { get; set; }
        }

        [Serializable]
        public class billinginfo
        {
            public string billingnumber { get; set; }
            public string billingcustomername { get; set; }
            public string billingstorenumber { get; set; }
            public string billingcity { get; set; }
            public string billingstate { get; set; }
            public string billingcountry { get; set; }
            public string billingzipcode { get; set; }
            public string billingaddress { get; set; }
            public string billingcontactname { get; set; }
            public string billingcontactphone { get; set; }
            public string billingcontactphoneextension { get; set; }
        }

    }
}