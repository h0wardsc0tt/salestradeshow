﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;

namespace Signature.Models
{
    public class SignatureModels
    {
        public class SignatureImageSave
        {
            public MemoryStream SaveSignatureImage(string signatureData)
            {
                var stream = new MemoryStream();
                using (MemoryStream mem = new MemoryStream(Convert.FromBase64String(signatureData)))
                {
                    Bitmap bm = (Bitmap)Image.FromStream(mem);
                    Bitmap resized = new Bitmap((int)(0.2f * bm.Width), (int)(0.2f * bm.Height));
                    Graphics g = Graphics.FromImage(resized);
                    g.DrawImage(bm, new Rectangle(0, 0, resized.Width, resized.Height), 0, 0, bm.Width, bm.Height, GraphicsUnit.Pixel);
                    g.Dispose();
                    //string path = Path.Combine(HttpContext.Current.Server.MapPath("../images"), "99444.png");
                    //resized.Save(path, ImageFormat.Png);
                    resized.Save(stream, ImageFormat.Png);
                    stream.Position = 0;
                }
                return stream;
            }
        }
        [Serializable]
        public class dto
        {
            public string image { get; set; }
            public string width { get; set; }
            public string height { get; set; }
            public string id { get; set; }
        }
    }
}